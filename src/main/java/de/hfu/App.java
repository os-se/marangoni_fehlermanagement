package de.hfu;

import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main( String[] args ) {
        System.out.println( "OSSE Praktikum Projekt" );

        System.out.println( "Zeichenkette eingeben:" );
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(input.toUpperCase());
    }
}