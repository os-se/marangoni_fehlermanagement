package de.hfu.unit;

public class Util {

	public static boolean istErstesHalbjahr(int monat){
		if ((monat < 1) || (monat > 12)) throw new IllegalArgumentException();
		if(monat <= 6) return true;//Korrigiert von Grenzwert 7 auf 6
		return false;
	}

}
