package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import de.hfu.integration.service.BaseResidentService;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class TestBaseResidentService {
    @Test
    public void testGetFilteredResidentsList() {
        BaseResidentService brs = new BaseResidentService();
        ResidentRepositoryStub rrs = new ResidentRepositoryStub();
        brs.setResidentRepository(rrs);
        List<Resident> allPossibleResidents = rrs.getResidents();

        //Speziellen Resident filtern
        List<Resident> result1 = brs.getFilteredResidentsList(allPossibleResidents.get(0));
        Assertions.assertEquals(result1.get(0), allPossibleResidents.get(0));

        //Pro Attribut Filtern
        Resident filterGivenName = new Resident("John*", null, null, null, null);
        Assertions.assertEquals(brs.getFilteredResidentsList(filterGivenName).get(0), allPossibleResidents.get(0));
        Resident filterFamilyName = new Resident(null, "Doe*", null, null, null);
        Assertions.assertEquals(brs.getFilteredResidentsList(filterFamilyName).get(0), allPossibleResidents.get(0));
        Resident filterStreet = new Resident(null, null, "JohnDoeStreet*", null, null);
        Assertions.assertEquals(brs.getFilteredResidentsList(filterStreet).get(0), allPossibleResidents.get(0));
        Resident filterCity = new Resident(null, null, null, "JohnDoeCity*", null);
        Assertions.assertEquals(brs.getFilteredResidentsList(filterCity).get(0), allPossibleResidents.get(0));
        Resident filterDate = new Resident(null, null, null, null, (new GregorianCalendar(1989, Calendar.SEPTEMBER, 26)).getTime());
        Assertions.assertEquals(brs.getFilteredResidentsList(filterDate).get(0), allPossibleResidents.get(0));

        //Alle Mustermanns
        Resident filterResident = new Resident(
                "*",
                "Mustermann",
                "*",
                "*",
                null
        );
        List<Resident> result2 = brs.getFilteredResidentsList(filterResident);
        Assertions.assertEquals(3, result2.size());

        //Keine Filter
        Resident filterResident2 = new Resident();
        List<Resident> result3 = brs.getFilteredResidentsList(filterResident2);
        Assertions.assertEquals(4, result3.size());
    }

    @Test
    public void testGetUniqueResident() {
        BaseResidentService brs = new BaseResidentService();
        ResidentRepositoryStub rrs = new ResidentRepositoryStub();
        brs.setResidentRepository(rrs);
        List<Resident> allPossibleResidents = rrs.getResidents();

        //Einen speziellen Resident
        try {
            Resident r1 = brs.getUniqueResident(allPossibleResidents.get(0));
            Assertions.assertEquals(r1, allPossibleResidents.get(0));
        } catch(ResidentServiceException rse) {
            Assertions.fail();
        }

        //Filter-Resident mit Wildcards
        Resident illegalFilterResident = new Resident("*", "*", "*", "*", new Date());
        Assertions.assertThrows(ResidentServiceException.class, () -> brs.getUniqueResident(illegalFilterResident));

        //Uneindeutige Residents
        Assertions.assertThrows(ResidentServiceException.class, () -> brs.getUniqueResident(allPossibleResidents.get(2)));
    }
}
