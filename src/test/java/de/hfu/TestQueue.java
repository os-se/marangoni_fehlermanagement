package de.hfu;

import de.hfu.unit.Queue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class TestQueue {
    @Test
    public void testCreateQueueWithIllegalLimit() {
        assertThrows(IllegalArgumentException.class, () -> new Queue(-1));
    }

    @Test
    public void testDequeOnEmptyQueueCausesException() {
        Queue queue = new Queue(3);
        assertThrows(IllegalStateException.class, queue::dequeue);
    }

    @Test
    public void testQueue() {
        //Queue anlegen
        Queue queue = new Queue(3);

        //In Queue aufnehmen
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);

        //Trotz voller Queue aufnehmen
        queue.enqueue(4);

        //Werte richtig aus Queue nehmen
        assertEquals(1, queue.dequeue());
        assertEquals(2, queue.dequeue());
        assertEquals(4, queue.dequeue());
    }
}
