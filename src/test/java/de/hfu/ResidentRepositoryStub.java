package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;

import java.util.ArrayList;
import java.util.List;

public class ResidentRepositoryStub implements ResidentRepository {
    private ArrayList<Resident> allResidents;

    public ResidentRepositoryStub() {
        allResidents = new ArrayList<>();

        Resident r1 = new Resident(
                "John",
                "Doe",
                "JohnDoeStreet",
                "JohnDoeCity",
                DateUtilityClass.generateDateFromString("26-09-1989")
        );
        Resident r2 = new Resident(
                "Max",
                "Mustermann",
                "Musterstrasse",
                "Musterstadt",
                DateUtilityClass.generateDateFromString("09-09-1999")
        );
        Resident r3 = new Resident(
                "Erika",
                "Mustermann",
                "Musterstrasse",
                "Musterstadt",
                DateUtilityClass.generateDateFromString("09-09-1999")
        );
        Resident r4 = new Resident(
                "Erika",
                "Mustermann",
                "Musterstrasse",
                "Musterstadt",
                DateUtilityClass.generateDateFromString("09-09-1999")
        );

        allResidents.add(r1);
        allResidents.add(r2);
        allResidents.add(r3);
        allResidents.add(r4);
    }
    @Override
    public List<Resident> getResidents() {
        return allResidents;
    }
}
