package de.hfu;

import de.hfu.unit.Util;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestUtil {
    @Test
    public void testIstErstesHalbjahr() {
        //Sinnlose Aenderung fuer Email-Test1
        assertTrue(Util.istErstesHalbjahr(1));
        assertTrue(Util.istErstesHalbjahr(6));
        assertFalse(Util.istErstesHalbjahr(7));
        assertFalse(Util.istErstesHalbjahr(12));

        assertThrows(IllegalArgumentException.class, () -> Util.istErstesHalbjahr(0));
        assertThrows(IllegalArgumentException.class, () -> Util.istErstesHalbjahr(13));
    }

    @Test
    public void testUtilConstructor() {
        Util util = new Util();
    }
}
