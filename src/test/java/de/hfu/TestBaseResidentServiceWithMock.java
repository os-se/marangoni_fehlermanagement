package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.easymock.EasyMock.*;

public class TestBaseResidentServiceWithMock {
    public static ResidentRepository rr;

    @BeforeAll
    static void initAll() {
        rr = createMock(ResidentRepository.class);

        Resident r1 = new Resident(
                "John",
                "Doe",
                "JohnDoeStreet",
                "JohnDoeCity",
                DateUtilityClass.generateDateFromString("26-09-1989")
        );
        Resident r2 = new Resident(
                "Max",
                "Mustermann",
                "Musterstrasse",
                "Musterstadt",
                DateUtilityClass.generateDateFromString("09-09-1999")
        );
        Resident r3 = new Resident(
                "Erika",
                "Mustermann",
                "Musterstrasse",
                "Musterstadt",
                DateUtilityClass.generateDateFromString("09-09-1999")
        );
        Resident r4 = new Resident(
                "Erika",
                "Mustermann",
                "Musterstrasse",
                "Musterstadt",
                DateUtilityClass.generateDateFromString("09-09-1999")
        );

        List<Resident> expectedResult = new ArrayList<>();
        expectedResult.add(r1);
        expectedResult.add(r2);
        expectedResult.add(r3);
        expectedResult.add(r4);

        expect(rr.getResidents()).andReturn(expectedResult).anyTimes();//anytime erlaubt rückgabe beliebig oft

        replay(rr);
    }

    /* Kopiert aus vorherigem Test: Jetzt mit Mock & Hamcrest */
    @Test
    public void testGetFilteredResidentsList() {
        BaseResidentService brs = new BaseResidentService();
        brs.setResidentRepository(rr);//neu eingefügt, vorheriges auskommentiert
        List<Resident> allPossibleResidents = rr.getResidents();

        //Speziellen Resident filtern
        List<Resident> result1 = brs.getFilteredResidentsList(allPossibleResidents.get(0));
        assertThat(result1.get(0), is(allPossibleResidents.get(0)));

        //Pro Attribut Filtern
        Resident filterGivenName = new Resident("John*", null, null, null, null);
        assertThat(brs.getFilteredResidentsList(filterGivenName).get(0), is(allPossibleResidents.get(0)));
        Resident filterFamilyName = new Resident(null, "Doe*", null, null, null);
        assertThat(brs.getFilteredResidentsList(filterFamilyName).get(0), is(allPossibleResidents.get(0)));
        Resident filterStreet = new Resident(null, null, "JohnDoeStreet*", null, null);
        assertThat(brs.getFilteredResidentsList(filterStreet).get(0), is(allPossibleResidents.get(0)));
        Resident filterCity = new Resident(null, null, null, "JohnDoeCity*", null);
        assertThat(brs.getFilteredResidentsList(filterCity).get(0), is(allPossibleResidents.get(0)));
        Resident filterDate = new Resident(null, null, null, null, (new GregorianCalendar(1989, Calendar.SEPTEMBER, 26)).getTime());
        assertThat(brs.getFilteredResidentsList(filterDate).get(0), is(allPossibleResidents.get(0)));

        //Alle Mustermanns
        Resident filterResident = new Resident(
                "*",
                "Mustermann",
                "*",
                "*",
                null
        );
        List<Resident> result2 = brs.getFilteredResidentsList(filterResident);
        assertThat(3, is(result2.size()));

        //Keine Filter
        Resident filterResident2 = new Resident();
        List<Resident> result3 = brs.getFilteredResidentsList(filterResident2);
        assertThat(4, is(result3.size()));
    }

    /* Kopiert aus vorherigem Test: Jetzt mit Mock & Hamcrest */
    @Test
    public void testGetUniqueResident() {
        BaseResidentService brs = new BaseResidentService();
        brs.setResidentRepository(rr);//neu eingefügt
        List<Resident> allPossibleResidents = rr.getResidents();

        //Einen speziellen Resident
        try {
            Resident r1 = brs.getUniqueResident(allPossibleResidents.get(0));
            assertThat(r1, is(allPossibleResidents.get(0)));
        } catch(ResidentServiceException rse) {
            Assertions.fail();
        }

        //Filter-Resident mit Wildcards
        Resident illegalFilterResident = new Resident("*", "*", "*", "*", new Date());
        Assertions.assertThrows(ResidentServiceException.class, () -> brs.getUniqueResident(illegalFilterResident));

        //Uneindeutige Residents
        Assertions.assertThrows(ResidentServiceException.class, () -> brs.getUniqueResident(allPossibleResidents.get(2)));
    }

    @AfterAll
    static void tearDownAll() {
        verify(rr);
    }
}
