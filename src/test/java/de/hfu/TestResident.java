package de.hfu;

import de.hfu.integration.domain.Resident;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class TestResident {
    @Test
    public void testResidentSettersGetters() {
        Resident testResident = new Resident();
        testResident.setGivenName("John");
        testResident.setFamilyName("Doe");
        testResident.setStreet("Johnstreet");
        testResident.setCity("Johncity");
        testResident.setDateOfBirth((new GregorianCalendar(2000, Calendar.FEBRUARY, 11)).getTime());

        Assertions.assertEquals("John", testResident.getGivenName());
        Assertions.assertEquals("Doe", testResident.getFamilyName());
        Assertions.assertEquals("Johnstreet", testResident.getStreet());
        Assertions.assertEquals("Johncity", testResident.getCity());
        Assertions.assertEquals((new GregorianCalendar(2000, Calendar.FEBRUARY, 11)).getTime(), testResident.getDateOfBirth());
    }
}
