package de.hfu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtilityClass {
    public static Date generateDateFromString(String date_string) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date;
        try {
            date = formatter.parse(date_string);
        } catch (ParseException ex) {
            date = new Date();
        }
        return date;
    }
}
