package de.hfu;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

public class TestRest {
    @Test
    public void testMainApp() {
        //Test Standardkonstruktor
        App app = new App();

        //Test Fake Input
        String input = "test";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        PrintStream originalErr = System.err;

        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        App.main(null);

        Assertions.assertEquals("OSSE Praktikum Projekt\n" +
                "Zeichenkette eingeben:\n" +
                "TEST\n", outContent.toString());

        System.setOut(originalOut);
        System.setErr(originalErr);
    }
}
